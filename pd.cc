
#include "led-matrix.h"

#include <unistd.h>
#include <math.h>
#include <stdio.h>
#include <string>

using rgb_matrix::GPIO;
using rgb_matrix::RGBMatrix;
using rgb_matrix::Canvas;


static void DrawEarth(Canvas *canvas){
    // draw a blue spot at the bottom of the screen to represent Urth
    int x = canvas->width() / 2;
    int y = canvas->height() - 1; 

    canvas->SetPixel(x, y, 0, 0, 255);
    canvas->SetPixel(x - 1, y, 0, 0, 255);
    canvas->SetPixel(x, y - 1, 0, 0, 255);
    canvas->SetPixel(x + 1, y, 0, 0, 255);
}

int main(int argc, char *argv[]) {
    /*
     * Set up GPIO pins. This fails when not running as root.
     */
    GPIO io;
    if (!io.Init()){
        printf("RPi pins not available");
        return 1;
    }
    
    //RGBMatrix(gpio, rows, chained displays, parallel displays)
    Canvas *canvas = new RGBMatrix(&io, 16, 1, 1);
    
    // First draw a nice background
    canvas->Fill(0,255,0);
    
    //now the earth
    DrawEarth(canvas);

    sleep(5);
    // Animation finished. Shut down the RGB matrix.
    canvas->Clear();
    delete canvas;

    return 0;
}
